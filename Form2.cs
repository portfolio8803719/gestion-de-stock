﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace ConnexionBaseMysql
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            InitConnection();
            RecupProduit();

        }

        private MySqlConnection connection;
        private MySqlCommand command;
        private MySqlDataReader reader;
        private string connectionString = "server=localhost;user id=root;database=gestionstock;SslMode=none";

        private void InitConnection()
        {
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open(); // connexion à la bdd
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(0);
            }
        }

        private void RecupProduit()
        {
            string query = "select * from materiel";
            command = new MySqlCommand(query, connection);
            command.Prepare();
            reader = command.ExecuteReader();
            listProd.Items.Clear();
            while (reader.Read())
            {
                listProd.Items.Add(reader["nom"]);
                listProd.Items.Add(reader["quantite"]);
            }
            reader.Close();
        }


        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void btnAjoutP_Click(object sender, EventArgs e)
        {

        }

        private void btnAjoutPG_Click(object sender, EventArgs e)
        {
            if (!txtNomP.Text.Equals("") && !txtQ.Text.Equals("") && !txtCateg.Text.Equals(""))
            {
                // construction de la requête
                string query = "insert into materiel (nom, quantite, categorie) value (@nom,@quantite,@categ)";
                //string query2 = "insert into materiel (quantite) value (@quantite)";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("@nom", txtNomP.Text);
                parameters.Add("@quantite", txtQ.Text);
                parameters.Add("@categ", txtCateg.Text);

                // exécution de la requête
                command = new MySqlCommand(query, connection);
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    command.Parameters.Add(new MySqlParameter(param.Key, param.Value));
                }
                command.Prepare();
                command.ExecuteNonQuery();
                // màj de la liste
                RecupProduit();
                txtNomP.Text = "";
                txtQ.Text = "";
                txtCateg.Text = "";

            }
        }

        private void btnSuppP_Click(object sender, EventArgs e)
        {
            if (listProd.SelectedIndex != -1)
            {
                // construction de la requête
                string query = "delete from materiel where nom = @nom";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("@nom", listProd.SelectedItem);
                // exécution de la requête
                command = new MySqlCommand(query, connection);
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    command.Parameters.Add(new MySqlParameter(param.Key, param.Value));
                }
                command.Prepare();
                command.ExecuteNonQuery();
                // màj de la liste
                RecupProduit();
            }
        }

        private void btnTriez_Click(object sender, EventArgs e)
        {
            string query = "select * from materiel order by nom asc";
            command = new MySqlCommand(query, connection);
            command.Prepare();
            reader = command.ExecuteReader();
            listProd.Items.Clear();
            while (reader.Read())
            {
                listProd.Items.Add(reader["nom"]);
                listProd.Items.Add(reader["quantite"]);
            }
            reader.Close();
        }

        private void btnModif_Click(object sender, EventArgs e)
        {
            if (listProd.SelectedIndex != -1)
            {
                // construction de la requête
                string query = "update materiel set nom = @nom where nom = @sel";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("@nom", txtNomP.Text);
                parameters.Add("@sel", listProd.SelectedItem);
                // exécution de la requête
                command = new MySqlCommand(query, connection);
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    command.Parameters.Add(new MySqlParameter(param.Key, param.Value));
                }
                command.Prepare();
                command.ExecuteNonQuery();
                // màj de la liste
                RecupProduit();
                txtNomP.Text = "";
            }
        }
            private void btnSupp_Click(object sender, EventArgs e)
            {
               
            }

        private void btnModifQ_Click(object sender, EventArgs e)
        {
            if (listProd.SelectedIndex != -1)
            {
                // construction de la requête
                string query = "update materiel set quantite = @quantite where nom = @sel";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("@quantite", txtQ.Text);
                parameters.Add("@sel", listProd.SelectedItem);
                // exécution de la requête
                command = new MySqlCommand(query, connection);
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    command.Parameters.Add(new MySqlParameter(param.Key, param.Value));
                }
                command.Prepare();
                command.ExecuteNonQuery();
                // màj de la liste
                RecupProduit();
                txtQ.Text = "";
            }
        }

        private void btnModifCateg_Click(object sender, EventArgs e)
        {
            if (listProd.SelectedIndex != -1)
            {
                // construction de la requête
                string query = "update materiel set categorie = @categorie where categorie = @sel";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("@categorie", txtCateg.Text);
                parameters.Add("@sel", listProd.SelectedItem);
                // exécution de la requête
                command = new MySqlCommand(query, connection);
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    command.Parameters.Add(new MySqlParameter(param.Key, param.Value));
                }
                command.Prepare();
                command.ExecuteNonQuery();
                // màj de la liste
                RecupProduit();
                txtCateg.Text = "";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
