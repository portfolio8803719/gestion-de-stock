﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ConnexionBaseMysql
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitConnection();
            RecupProfils();

        }

        private MySqlConnection connection;
        private MySqlCommand command;
        private MySqlDataReader reader;
        private string connectionString = "server=localhost;user id=root;database=gestionstock;SslMode=none";

        private void InitConnection()
        {
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open(); // connexion à la bdd
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(0);
            }
        }

        private void RecupProfils()
        {
            string query = "select * from fournisseur";
            command = new MySqlCommand(query, connection);
            command.Prepare();
            reader = command.ExecuteReader();
            listBox1.Items.Clear();
            while (reader.Read())
            {
                listBox1.Items.Add(reader["nom"]);
            }
            reader.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!txtNom.Text.Equals(""))
            {
                // construction de la requête
                string query = "insert into fournisseur (nom) value (@nom)";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("@nom", txtNom.Text);
                // exécution de la requête
                command = new MySqlCommand(query, connection);
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    command.Parameters.Add(new MySqlParameter(param.Key, param.Value));
                }
                command.Prepare();
                command.ExecuteNonQuery();
                // màj de la liste
                RecupProfils();
                txtNom.Text = "";
            }

        }

        private void btnSupp_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                // construction de la requête
                string query = "delete from fournisseur where nom = @nom";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("@nom", listBox1.SelectedItem);
                // exécution de la requête
                command = new MySqlCommand(query, connection);
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    command.Parameters.Add(new MySqlParameter(param.Key, param.Value));
                }
                command.Prepare();
                command.ExecuteNonQuery();
                // màj de la liste
                RecupProfils();
            }

        }

        private void btnTriAZ_Click(object sender, EventArgs e)
        {
            string query = "select * from fournisseur order by nom asc";
            command = new MySqlCommand(query, connection);
            command.Prepare();
            reader = command.ExecuteReader();
            listBox1.Items.Clear();
            while (reader.Read())
            {
                listBox1.Items.Add(reader["nom"]);
            }
            reader.Close();

        }

        private void btnProd_Click(object sender, EventArgs e)
        {
            Form2 Form2 = new Form2();
            Form2.Show();
        }
    }
}
/*MySqlConnection connexion = new MySqlConnection("Server=localhost;Database=gestionstock;Uid=root;Pwd=;");
            try
            {
                // Console.WriteLine("Openning Connection ...");

                connexion.Open();
                MessageBox.Show("success");
            }
            catch (Exception f)
            {
                MessageBox.Show("down");
            }

            Console.Read();
            */
