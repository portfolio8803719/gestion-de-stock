﻿namespace ConnexionBaseMysql
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAjout = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnSupp = new System.Windows.Forms.Button();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.btnTriAZ = new System.Windows.Forms.Button();
            this.lbl_F = new System.Windows.Forms.Label();
            this.btnProd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAjout
            // 
            this.btnAjout.ForeColor = System.Drawing.Color.Blue;
            this.btnAjout.Location = new System.Drawing.Point(256, 420);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(92, 23);
            this.btnAjout.TabIndex = 0;
            this.btnAjout.Text = "ajouter";
            this.btnAjout.UseVisualStyleBackColor = true;
            this.btnAjout.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(36, 68);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(201, 308);
            this.listBox1.TabIndex = 1;
            // 
            // btnSupp
            // 
            this.btnSupp.ForeColor = System.Drawing.Color.Red;
            this.btnSupp.Location = new System.Drawing.Point(365, 420);
            this.btnSupp.Name = "btnSupp";
            this.btnSupp.Size = new System.Drawing.Size(102, 23);
            this.btnSupp.TabIndex = 2;
            this.btnSupp.Text = "supprimer";
            this.btnSupp.UseVisualStyleBackColor = true;
            this.btnSupp.Click += new System.EventHandler(this.btnSupp_Click);
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(36, 421);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(201, 22);
            this.txtNom.TabIndex = 3;
            // 
            // btnTriAZ
            // 
            this.btnTriAZ.Location = new System.Drawing.Point(268, 68);
            this.btnTriAZ.Name = "btnTriAZ";
            this.btnTriAZ.Size = new System.Drawing.Size(92, 23);
            this.btnTriAZ.TabIndex = 4;
            this.btnTriAZ.Text = "triez A-Z";
            this.btnTriAZ.UseVisualStyleBackColor = true;
            this.btnTriAZ.Click += new System.EventHandler(this.btnTriAZ_Click);
            // 
            // lbl_F
            // 
            this.lbl_F.AutoSize = true;
            this.lbl_F.Location = new System.Drawing.Point(36, 28);
            this.lbl_F.Name = "lbl_F";
            this.lbl_F.Size = new System.Drawing.Size(135, 16);
            this.lbl_F.TabIndex = 5;
            this.lbl_F.Text = "Liste des fourniseurs :";
            // 
            // btnProd
            // 
            this.btnProd.Location = new System.Drawing.Point(202, 462);
            this.btnProd.Name = "btnProd";
            this.btnProd.Size = new System.Drawing.Size(248, 23);
            this.btnProd.TabIndex = 6;
            this.btnProd.Text = "Voir les produits";
            this.btnProd.UseVisualStyleBackColor = true;
            this.btnProd.Click += new System.EventHandler(this.btnProd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 402);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nom du nouveau fournisseur :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 521);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnProd);
            this.Controls.Add(this.lbl_F);
            this.Controls.Add(this.btnTriAZ);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.btnSupp);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnAjout);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAjout;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnSupp;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Button btnTriAZ;
        private System.Windows.Forms.Label lbl_F;
        private System.Windows.Forms.Button btnProd;
        private System.Windows.Forms.Label label1;
    }
}

