﻿namespace ConnexionBaseMysql
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listProd = new System.Windows.Forms.ListBox();
            this.txtNomP = new System.Windows.Forms.TextBox();
            this.txtQ = new System.Windows.Forms.TextBox();
            this.btnAjoutPG = new System.Windows.Forms.Button();
            this.txtCateg = new System.Windows.Forms.TextBox();
            this.btnSuppP = new System.Windows.Forms.Button();
            this.btnTriez = new System.Windows.Forms.Button();
            this.btnModif = new System.Windows.Forms.Button();
            this.btnModifQ = new System.Windows.Forms.Button();
            this.btnModifCateg = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listProd
            // 
            this.listProd.FormattingEnabled = true;
            this.listProd.ItemHeight = 16;
            this.listProd.Location = new System.Drawing.Point(114, 35);
            this.listProd.Name = "listProd";
            this.listProd.Size = new System.Drawing.Size(184, 292);
            this.listProd.TabIndex = 0;
            // 
            // txtNomP
            // 
            this.txtNomP.Location = new System.Drawing.Point(338, 35);
            this.txtNomP.Name = "txtNomP";
            this.txtNomP.Size = new System.Drawing.Size(154, 22);
            this.txtNomP.TabIndex = 2;
            // 
            // txtQ
            // 
            this.txtQ.Location = new System.Drawing.Point(509, 35);
            this.txtQ.Name = "txtQ";
            this.txtQ.Size = new System.Drawing.Size(142, 22);
            this.txtQ.TabIndex = 4;
            // 
            // btnAjoutPG
            // 
            this.btnAjoutPG.ForeColor = System.Drawing.Color.Blue;
            this.btnAjoutPG.Location = new System.Drawing.Point(397, 148);
            this.btnAjoutPG.Name = "btnAjoutPG";
            this.btnAjoutPG.Size = new System.Drawing.Size(380, 23);
            this.btnAjoutPG.TabIndex = 5;
            this.btnAjoutPG.Text = "Ajouter";
            this.btnAjoutPG.UseVisualStyleBackColor = true;
            this.btnAjoutPG.Click += new System.EventHandler(this.btnAjoutPG_Click);
            // 
            // txtCateg
            // 
            this.txtCateg.Location = new System.Drawing.Point(671, 34);
            this.txtCateg.Name = "txtCateg";
            this.txtCateg.Size = new System.Drawing.Size(142, 22);
            this.txtCateg.TabIndex = 6;
            // 
            // btnSuppP
            // 
            this.btnSuppP.ForeColor = System.Drawing.Color.Red;
            this.btnSuppP.Location = new System.Drawing.Point(376, 290);
            this.btnSuppP.Name = "btnSuppP";
            this.btnSuppP.Size = new System.Drawing.Size(116, 37);
            this.btnSuppP.TabIndex = 8;
            this.btnSuppP.Text = "Supprimer";
            this.btnSuppP.UseVisualStyleBackColor = true;
            this.btnSuppP.Click += new System.EventHandler(this.btnSuppP_Click);
            // 
            // btnTriez
            // 
            this.btnTriez.Location = new System.Drawing.Point(12, 33);
            this.btnTriez.Name = "btnTriez";
            this.btnTriez.Size = new System.Drawing.Size(95, 23);
            this.btnTriez.TabIndex = 9;
            this.btnTriez.Text = "Triez A-Z";
            this.btnTriez.UseVisualStyleBackColor = true;
            this.btnTriez.Click += new System.EventHandler(this.btnTriez_Click);
            // 
            // btnModif
            // 
            this.btnModif.Location = new System.Drawing.Point(535, 290);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(135, 50);
            this.btnModif.TabIndex = 10;
            this.btnModif.Text = "Modifier le nom";
            this.btnModif.UseVisualStyleBackColor = true;
            this.btnModif.Click += new System.EventHandler(this.btnModif_Click);
            // 
            // btnModifQ
            // 
            this.btnModifQ.Location = new System.Drawing.Point(535, 356);
            this.btnModifQ.Name = "btnModifQ";
            this.btnModifQ.Size = new System.Drawing.Size(135, 45);
            this.btnModifQ.TabIndex = 11;
            this.btnModifQ.Text = "Modifier la quantite";
            this.btnModifQ.UseVisualStyleBackColor = true;
            this.btnModifQ.Click += new System.EventHandler(this.btnModifQ_Click);
            // 
            // btnModifCateg
            // 
            this.btnModifCateg.Location = new System.Drawing.Point(535, 417);
            this.btnModifCateg.Name = "btnModifCateg";
            this.btnModifCateg.Size = new System.Drawing.Size(135, 45);
            this.btnModifCateg.TabIndex = 12;
            this.btnModifCateg.Text = "Modifier la categorie";
            this.btnModifCateg.UseVisualStyleBackColor = true;
            this.btnModifCateg.Click += new System.EventHandler(this.btnModifCateg_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(338, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 40);
            this.label1.TabIndex = 13;
            this.label1.Text = "Entrez le nom*";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(668, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 40);
            this.label2.TabIndex = 14;
            this.label2.Text = "Entrez la catégorie*";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(506, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 40);
            this.label3.TabIndex = 15;
            this.label3.Text = "Entrez la quantité*";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(335, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(273, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "(*) Champ obligatoire pour l\'ajout d\'un produit";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 524);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnModifCateg);
            this.Controls.Add(this.btnModifQ);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.btnTriez);
            this.Controls.Add(this.btnSuppP);
            this.Controls.Add(this.txtCateg);
            this.Controls.Add(this.btnAjoutPG);
            this.Controls.Add(this.txtQ);
            this.Controls.Add(this.txtNomP);
            this.Controls.Add(this.listProd);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listProd;
        private System.Windows.Forms.TextBox txtNomP;
        private System.Windows.Forms.TextBox txtQ;
        private System.Windows.Forms.Button btnAjoutPG;
        private System.Windows.Forms.TextBox txtCateg;
        private System.Windows.Forms.Button btnSuppP;
        private System.Windows.Forms.Button btnTriez;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.Button btnModifQ;
        private System.Windows.Forms.Button btnModifCateg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}